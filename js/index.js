function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;} //Data
var data = {
  baseURL: "https://day-7-messaging.firebaseio.com/",
  profileImage: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/2804273/caballo.jpg",
  messages: {},
  prevChats: [
  {
    lastText: "",
    sender: {
      name: "SM Designers Chatroom",
      profileImage: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/2804273/SM_Logo_Shadow.png" } },


  {
    lastText: "Are you at CCI makerspace?",
    sender: {
      name: "Corey",
      profileImage: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/2804273/Corey.jpg" } },


  {
    lastText: "Does anyone knows how to use the 3D printer?",
    sender: {
      name: "Maria 💗",
      profileImage: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/2804273/Maria.jpg" } },


  {
    lastText: "Mark sent a photo.",
    sender: {
      name: "Mark",
      profileImage: "https://zephyo.github.io/22Days/code/7/graphics/users.svg" } }] },




msgName = "messages",
config = {
  apiKey: "AIzaSyBnWbqZC0wncY06pWlHX8DCbIM_EM9zrE8",
  authDomain: "day-7-messaging.firebaseapp.com",
  databaseURL: "https://day-7-messaging.firebaseio.com",
  projectId: "day-7-messaging",
  storageBucket: "day-7-messaging.appspot.com",
  messagingSenderId: "307150346579" };


firebase.initializeApp(config);

//Components

//App Container
class App extends React.Component {
  constructor(props) {
    super(props);_defineProperty(this, "update",









    () =>
    {
      this.state.firebaseRef.on("value",
      snapshot => {
        this.setState({
          messages: snapshot.val() },
        () => {
          if (this.state.messages)
          {
            var last = Object.keys(this.state.messages)[Object.keys(this.state.messages).length - 1];
            var prev = this.state.prevChats;
            prev[0].lastText = this.state.messages[last].message;
            this.setState({
              prevChats: prev });

            this.scrollWindow();
          }
        });
      });
    });_defineProperty(this, "login",

    () => {
      var user = {
        name: $("#name").val(),
        email: $("#email").val() };

      if (user.name && user.email) {
        this.setState({ thisUser: user });
      }
    });_defineProperty(this, "handleSubmit",

    () => {
      var value = $(".messageInput").val();
      if (value.length === 0) {

        return;
      }
      var message = {
        sender: this.state.thisUser,
        message: value };


      this.postMessage(message);
      $(".messageInput").val("");
    });_defineProperty(this, "scrollWindow",








    () => {
      var windowHeight = $(".Messages").height();
      $(".container").animate(
      { scrollTop: $(".container")[0].scrollHeight },
      500);

    });this.state = data;}componentWillMount() {this.setState({ firebaseRef: firebase.database().ref(msgName) }, this.update);}postMessage(message) {// Get a key for a new Post.
    var newPostRef = this.state.firebaseRef.push();newPostRef.set(message, this.scrollWindow);}
  render() {
    return (
      React.createElement("div", { className: "App" },
      React.createElement(Conversation, {
        messages: this.state.messages,
        recipient: this.state.recipient,
        onSubmit: this.handleSubmit,
        thisUser: this.state.thisUser }),

      React.createElement(Profile, {
        thisUser: this.state.thisUser,
        prevChats: this.state.prevChats,
        profileImage: this.state.profileImage }),

      !this.state.thisUser ? React.createElement(LoginForm, { login: this.login }) : null));


  }}


const LoginForm = props => {
  return (
    React.createElement("div", { className: "LoginForm" },
    React.createElement("div", { className: "sign-up" },
    React.createElement("h1", null, "Login"),
    React.createElement("div", { className: "input" }, React.createElement("input", { id: "name", type: "text", placeholder: "Name" }), React.createElement("i", { "data-feather": "user" })),
    React.createElement("div", { className: "input" }, React.createElement("input", { id: "email", type: "email", placeholder: "Email" }), React.createElement("i", { "data-feather": "mail" })),
    React.createElement("button", { onClick: props.login }, "LET'S CHAT & Make a Statement!"))));



};

//Conversation
class Conversation extends React.Component {
  constructor(props) {
    super(props);_defineProperty(this, "onChange",


    () => {
      if ($('.messageInput').val().length > 0) {
        $('#send-button').css({
          "background": "#fad0c4" });
      } else {
        $('#send-button').css({
          "background": "white" });
      }
    });}

  render()
  {return (
      React.createElement("div", { className: "Conversation" },
      React.createElement(Header, { name: "SM Designers Chatroom" }),
      React.createElement("div", { className: "container" },
      React.createElement(Messages, { messages: this.props.messages, thisUser: this.props.thisUser })),

      React.createElement("input", {
        className: "messageInput",
        name: "message",
        type: "text",
        placeholder: "type something",
        onChange: this.onChange }),

      React.createElement("button", { id: "send-button", onClick: this.props.onSubmit },
      React.createElement("i", { "data-feather": "send" }))));



  }}


//Profile
var Profile = props => {
  var prevchats = props.prevChats.map(function (chat, i) {
    return React.createElement(PastChat, { lastText: chat.lastText, sender: chat.sender });
  });
  return (
    React.createElement("div", { className: "Profile" },
    React.createElement("div", { className: "top-half" },
    React.createElement("h1", null, props.thisUser ? props.thisUser.name : "", " "),
    React.createElement("p", { className: "email" }, props.thisUser ? props.thisUser.email : ""),
    React.createElement("img", { className: "profileImage main", src: props.profileImage }),
    React.createElement("div", { className: "icons" },
    React.createElement("a", { href: "#" }, React.createElement("i", { "data-feather": "alert-circle" })),
    React.createElement("a", { href: "#", className: "active" }, React.createElement("i", { "data-feather": "message-circle" })),
    React.createElement("a", { href: "#" }, React.createElement("i", { "data-feather": "home" })),
    React.createElement("a", { href: "#" }, React.createElement("i", { "data-feather": "activity" })))),


    React.createElement("div", { className: "prev-chats" }, prevchats)));


};

var PastChat = props => {
  return (
    React.createElement("div", { className: "prev-chat" },
    React.createElement("img", { className: "profileImage", src: props.sender.profileImage }),
    React.createElement("h2", null, props.sender.name),
    React.createElement("p", null, props.lastText)));


};

var Header = props => {
  return (
    React.createElement("header", null,
    React.createElement("i", { "data-feather": "chevron-left" }),
    React.createElement("div", { className: "name" }, props.name)));


};


//Messages
var Messages = props => {
  let messages = [];
  for (var key in props.messages) {
    var message = props.messages[key];
    messages.push(
    React.createElement(Message, {
      message: message.message,
      sender: message.sender,
      thisUser: props.thisUser }));


  }

  return React.createElement("div", { className: "Messages" }, messages);
};

//Message
var Message = props => {
  var classNames = "";
  if (props.thisUser && props.sender.name == props.thisUser.name && props.sender.email == props.thisUser.email) {
    classNames = "Message you";
  } else {
    classNames = "Message them";
  }

  return (
    React.createElement("div", { className: classNames },
    React.createElement("span", null, props.message),
    React.createElement("p", null, props.sender.name)));


};

//Render
ReactDOM.render(React.createElement(App, null), document.getElementById("app"));

feather.replace();